<?php
namespace App\Entity;

use App\Helpers\Traits\Contextable;
use App\Repository\DevisRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Philemon Globlehi <philemon.globlehi@gmail.com>
 *
 * @ORM\Entity(repositoryClass=DevisRepository::class)
 */
class Devis
{
    use Contextable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class, inversedBy="devis")
     */
    private $products;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalAmountWithoutDiscount;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalAmountWithDiscount;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalProductsDiscount;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        $this->products->removeElement($product);

        return $this;
    }

    public function getTotalAmountWithoutDiscount(): ?int
    {
        return $this->totalAmountWithoutDiscount;
    }

    public function setTotalAmountWithoutDiscount(int $totalAmountWithoutDiscount): self
    {
        $this->totalAmountWithoutDiscount = $totalAmountWithoutDiscount;

        return $this;
    }

    public function getTotalAmountWithDiscount(): ?int
    {
        return $this->totalAmountWithDiscount;
    }

    public function setTotalAmountWithDiscount(int $totalAmountWithDiscount): self
    {
        $this->totalAmountWithDiscount = $totalAmountWithDiscount;

        return $this;
    }

    public function getTotalProductsDiscount(): ?int
    {
        return $this->totalProductsDiscount;
    }

    public function setTotalProductsDiscount(int $totalProductsDiscount): self
    {
        $this->totalProductsDiscount = $totalProductsDiscount;

        return $this;
    }
}
