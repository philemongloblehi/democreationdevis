<?php
namespace App\Controller;

use App\Entity\Devis;
use App\Entity\Product;
use App\Service\DiscountCalculation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Philemon Globlehi <philemon.globlehi@gmail.com>
 *
 * @Route(name="api_devis_", path="/devis")
 *
 * Class DevisController
 * @package App\Controller
 */
class DevisController extends AbstractController
{

    /**
     * Create a devis
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     * @Route(name="create", methods={"POST"})
     */
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $requestContent = json_decode($request->getContent(), true);
        $user = static::edit(new Devis(), $requestContent);

        $errors = $this->validate($user, null, ['Create']);
        if (null !== $errors) {
            return $errors;
        }

        $em->persist($user);
        $em->flush();

        return $this->renderResponse($user, ['details'], 201);

    }

    public static function edit(Devis $devis, array $requestContent = []) :Devis
    {
        if (true === isset($requestContent['libelle'])) {
            $devis->setLibelle($requestContent['libelle']);
        }
        if (true === isset($requestContent['description'])) {
            $devis->setDescription($requestContent['description']);
        }
        if (true === isset($requestContent['products'])) {
            $totalAmountProductsDiscount = 0;
            $totalAmountProducts = 0;
            /** @var Product $product */
            foreach ($requestContent['products'] as $product) {
                if (null !== $product->getDiscount()) {
                    $amountProductDiscount = DiscountCalculation::getAmountDiscount($product->getAmount(), $product->getDiscount()->getReduction());
                    $totalAmountProductsDiscount += $amountProductDiscount;
                }

                $totalAmountProducts += $product->getAmount();
                $devis->addProduct($product);
            }

            $devis->setTotalProductsDiscount($totalAmountProductsDiscount);
            $devis->setTotalAmountWithoutDiscount($totalAmountProducts);
            $devis->setTotalAmountWithoutDiscount($totalAmountProducts - $totalAmountProductsDiscount);
        }

        return $devis;
    }

    public function renderResponse(Devis $devis, array $serializationGroups = ['details'], int $statusCode = 200, array $headers = []) :Response
    {
        return new JsonResponse($this->get('jms_serializer')->toArray($devis, Devis::createSerializationContext($serializationGroups)), $statusCode, $headers);
    }

    public function validate(Devis $devis, Constraint $constraint = null, array $groups = []): ?Response
    {
        /**
         * @var ConstraintViolationListInterface $errors
         */
        $errors = $this->get('validator')->validate($devis, $constraint, $groups);

        if (0 < $errors->count()) {
            return new JsonResponse(['errors' => $errors, 'message' => 'Les paramêtres fournis pour la requête ne sont pas valides.'], 400);
        }

        return null;
    }

    public static function getSubscribedServices(): array
    {
        return array_merge(parent::getSubscribedServices(), [
            'validator' => '?'.ValidatorInterface::class,
            'translator' => '?'.TranslatorInterface::class,
            'jms_serializer' => '?'.SerializerInterface::class,
            'doctrine.orm.entity_manager' => '?'.EntityManagerInterface::class,
        ]);
    }



}
